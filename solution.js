const cards = require('./p3_1.js');

// Each question required only one hof function to solve.
// Use method chaining to solve #3, #4, #5, #6 and #7.

// 1. Find all card numbers whose sum of all the even position digits is odd.
function sumOfEvenPositionIsOdd(cards) {
    let cardNumbers = [];

    cardNumbers = cards.reduce((cardNumbers, card) => {
        let sumOfEvenPositions = card["card_number"].split("").reduce((sumOfEvenPositions, number, index) => {
            if ( index % 2 === 0) {
                sumOfEvenPositions += Number(number);
            }

            return sumOfEvenPositions;
        }, 0);

        if ( sumOfEvenPositions % 2 !== 0) {
            cardNumbers.push(card["card_number"]);
        }

        return cardNumbers;
    }, []);

    return cardNumbers;
}

// console.log(sumOfEvenPositionIsOdd(cards));


// 2. Find all cards that were issued before June.
function cardsIssuedBeforeJune(cards) {
    let cardsCreatedBeforeJune = [];

    cardsCreatedBeforeJune = cards.filter((card) => {
        let issueMonth = Number(card.issue_date.split("/")[0]);
    
        return issueMonth < 6;
    });

    return cardsCreatedBeforeJune;
}

// console.log(cardsIssuedBeforeJune(cards));

// 3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
function assignCVV(cards) {
    cards.reduce((cards, card, index) => {
        let randomNumber = Math.floor(Math.random() * (999 - 100 + 1) + 100);

        cards[index]["cvv"] = randomNumber;

        return cards
    },cards);

    return cards;
}

// console.log(assignCVV(cards));

// 4. Add a new field to each card to indicate if the card is valid or not.
function addValidity(cards) {
    cards.reduce((cards, card, index) => {
        // 0 is invalid, 1 is valid
        // cards[index]["validity"] = Math.floor(Math.random() * 2);
        
        // all cards will be valid by default
        cards[index]["validity"] = 1;

        return cards;
    }, cards);

    return cards;
}
// cards = addValidity(cards)
// console.log();
// console.log(cards);

// 0 is invalid, 1 is valid
// 5. Invalidate all cards issued before March.
function invalidateCardsBeforeMarch(cards) {
    addValidity(cards)
    .reduce((cards, card, index) => {
        let month = Number(card.issue_date.split("/")[0]);

        if ( month < 3) {
            cards[index]["validity"] = 0;
        }

        return cards;
    }, cards);

    return cards;
}

// console.log(invalidateCardsBeforeMarch(cards));

// 6. Sort the data into ascending order of issue date.

// helper function to parse date
function formatDateForParsing(date) {
    // date arg is in mm/dd/yyyy
    date = date.split("/");

    // convert to yyyy-mm-dd
    return Date.parse(date[2] + "-" + date[0] + "-" + date[1]);
}


function sortByIssueDate(cards) {
    let sortedByIssueDate = cards.sort((a, b) => {
        let aIssueDate = formatDateForParsing(a.issue_date);
        let bIssueDate = formatDateForParsing(b.issue_date);

        return aIssueDate - bIssueDate;
    });

    return sortedByIssueDate;
}

// console.log(sortByIssueDate(cards));

// 7. Group the data in such a way that we can identify which cards were assigned in which months.
// [{"id":1,"card_number":"5602221055053843723","card_type":"china-unionpay","issue_date":"5/25/2021","salt":"x6ZHoS0t9vIU","phone":"339-555-5239"},
// {"id":2,"card_number":"3547469136425635","card_type":"jcb","issue_date":"12/18/2021","salt":"FVOUIk","phone":"847-313-1289"}]

function groupByIssuedMonth(cards) {
    let groupedByMonth = {};
    const lookupMonthNames = ["Jan", "Feb", "March", "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
    "Oct", "Nov", "Dec"];

    groupedByMonth = cards.reduce((groupedByMonth, card) => {
        let month = Number(card["issue_date"].split("/")[0]);

        if ( groupedByMonth.hasOwnProperty(lookupMonthNames[month - 1]) === false) {
            groupedByMonth[lookupMonthNames[month - 1]] = [];
        }
        groupedByMonth[lookupMonthNames[month - 1]].push(card);

        return groupedByMonth;
    }, {});

    return groupedByMonth;
}

console.log(groupByIssuedMonth(cards));